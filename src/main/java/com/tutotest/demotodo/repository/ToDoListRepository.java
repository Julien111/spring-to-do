/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutotest.demotodo.repository;

import com.tutotest.demotodo.entity.ToDoList;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Julien
 */
public interface ToDoListRepository extends CrudRepository<ToDoList, Long> {
    ToDoList findById(long id);    
}
