/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutotest.demotodo.controller;

import com.tutotest.demotodo.exception.UserAlreadyExistException;
import com.tutotest.demotodo.service.UserServiceImpl;
import com.tutotest.demotodo.web.dto.UserRegistrationDto;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author Julien
 */
@Controller
public class RegistrationController {
    
    private UserServiceImpl userService;    
    
    @GetMapping("/register")
    public String register(Model model){
        model.addAttribute("userDto", new UserRegistrationDto());
        return "account/register";
    }
    
    @PostMapping("/register")
    public String userRegistration(@Valid @ModelAttribute("userDto") UserRegistrationDto userDto, BindingResult bindingResult, Model model){
        userDto.setRoles("ROLE_USER");
        System.out.println("test !!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        
        if(bindingResult.hasErrors() || !(userDto.getPassword().equals(userDto.getRepeatPassword()))){
            model.addAttribute("userDto", userDto);
            System.out.println("test save");
            return "account/register";
        }
        try{
            System.out.println("test save");
            userService.save(userDto);
        }catch (UserAlreadyExistException e){
            System.out.println("error save");
            bindingResult.rejectValue("email", "userForm.email","Cet email est déjà utilisé pour un autre compte.");
            model.addAttribute("userDto", userDto);
            return "account/register";
        }
        return "redirect:account/profil";
    }
    
    @GetMapping("/profil")
    public String profil(Model model){        
        return "account/profil";
    }
    
}
