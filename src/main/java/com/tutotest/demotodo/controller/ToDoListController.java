/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutotest.demotodo.controller;

import javax.validation.Valid;

import com.tutotest.demotodo.entity.ToDoList;
import com.tutotest.demotodo.service.ToDoListService;
import com.tutotest.demotodo.validatingforminput.ToDoListForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Julien
 */
@Controller
public class ToDoListController implements WebMvcConfigurer {    
        
    @Autowired
    private ToDoListService toDoListService;   
    
    
    @GetMapping("/todolists")
    public String getAllToDo(Model model){       
       model.addAttribute("tasks", toDoListService.getToDoLists());
       return "listtasks";
    }
    
    @GetMapping("/create-task")
    public String createTask(Model model){        
        ToDoListForm taskForm = new ToDoListForm();
        model.addAttribute("taskForm", taskForm);
        return "create";
    }
    
    @PostMapping("/create-task")
    public String taskSubmit(@Valid @ModelAttribute("taskForm") ToDoListForm taskForm, BindingResult bindingResult, Model model, RedirectAttributes redirectAttrs) { 
        if (bindingResult.hasErrors()) {            
            return "create";           
	}       
        ToDoList task = new ToDoList();
        task.setNameTask(taskForm.getNameTask());
        task.setTypeTask(taskForm.getTypeTask());
        task.setDescription(taskForm.getDescription());
        task.setDateEnd(taskForm.getDateEnd());
        
        toDoListService.createTask(task);
        redirectAttrs.addAttribute("task", task).addFlashAttribute("msgSuccessAdd", "La tâche a été ajoutée.");
        return "redirect:/todolists";
    }
    
    @GetMapping("/edit/{id}")
    public String editTask(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttrs){
        ToDoList task = toDoListService.getToDoById(id);
        
        if(!toDoListService.existsById(id)){      
           redirectAttrs.addAttribute("id", id).addFlashAttribute("errorId", "La tâche n'existe pas.");
           return "redirect:/todolists";
        }
        
        ToDoListForm taskForm = new ToDoListForm();
        taskForm.setNameTask(task.getNameTask());
        taskForm.setTypeTask(task.getTypeTask());
        taskForm.setDescription(task.getDescription());
        taskForm.setDateEnd(task.getDateEnd());        
        
        model.addAttribute("taskForm", taskForm);
        model.addAttribute("task_id", task.getId());
        return "edit";
    }
    
    @PostMapping("/edit/{id}")
    public String editSubmit(@PathVariable("id") Long id, @Valid @ModelAttribute("taskForm") ToDoListForm taskForm, BindingResult bindingResult, Model model, RedirectAttributes redirectAttrs) { 
        if (bindingResult.hasErrors()) {   
            model.addAttribute("taskForm", taskForm);
            model.addAttribute("task_id", id);
            return "edit";           
	}       
        ToDoList task = toDoListService.getToDoById(id);
        task.setNameTask(taskForm.getNameTask());
        task.setTypeTask(taskForm.getTypeTask());
        task.setDescription(taskForm.getDescription());
        task.setDateEnd(taskForm.getDateEnd());
        
        toDoListService.updateTask(task);        
        redirectAttrs.addAttribute("id", id).addFlashAttribute("msgSuccessEdit", "La tâche a bien été modifiée.");
        return "redirect:/todolists";
    }
    
    @GetMapping("/delete/{id}")    
    public String deleteTheTask(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttrs){
       ToDoList task = toDoListService.getToDoById(id);
        
       if(!toDoListService.existsById(id)){      
          redirectAttrs.addAttribute("id", id).addFlashAttribute("errorId", "La tâche n'existe pas.");
          return "redirect:/todolists";
       }
       toDoListService.deleteTask(task);
       redirectAttrs.addAttribute("id", id).addFlashAttribute("msgSuccessDelete", "La tâche a été supprimée.");
       return "redirect:/todolists";
    }
}
