/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutotest.demotodo.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Julien
 */
@Entity
@Table(name="todolist")
public class ToDoList {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Column(name = "nameTask")
    private String nameTask;
    
    @Column(name = "typeTask")
    private String typeTask;
    
    @Column(name="description", columnDefinition="TEXT")
    private String description;
    
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "dateEnd")
    private Date dateEnd;
    
    public ToDoList(){     
    }      

    public Long getId() {
        return id;
    }

    public String getNameTask() {
        return nameTask;
    }

    public String getTypeTask() {
        return typeTask;
    }

    public String getDescription() {
        return description;
    }    

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNameTask(String nameTask) {
        this.nameTask = nameTask;
    }

    public void setTypeTask(String typeTask) {
        this.typeTask = typeTask;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }  
    
}
