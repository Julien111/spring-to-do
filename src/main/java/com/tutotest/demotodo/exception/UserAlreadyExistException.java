/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutotest.demotodo.exception;

/**
 *
 * @author Julien
 */
public class UserAlreadyExistException extends Exception {
    public UserAlreadyExistException(String errorMessage){
        super(errorMessage);
    }
}
