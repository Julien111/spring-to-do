/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutotest.demotodo.service;

import com.tutotest.demotodo.entity.User;
import com.tutotest.demotodo.exception.UserAlreadyExistException;
import com.tutotest.demotodo.repository.UserRepository;
import com.tutotest.demotodo.web.dto.UserRegistrationDto;
import org.springframework.stereotype.Service;

/**
 *
 * @author Julien
 */
@Service
public class UserServiceImpl implements UserService {
    
    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }   
    
    @Override
    public User save(UserRegistrationDto registrationDto) throws UserAlreadyExistException {
       if(checkIfUserExist(registrationDto.getEmail())){
            throw new UserAlreadyExistException("Ce mail est déjà utilisé pour un compte.");
        }
       User user = new User(registrationDto.getFirstName(), registrationDto.getLastName(), registrationDto.getEmail(), registrationDto.getPassword(), registrationDto.getRoles());       
       return userRepository.save(user); 
    }
    
    @Override
    public boolean checkIfUserExist(String email) {
        if(userRepository.findByEmail(email) !=null){
            return true;
        }
        else{
            return false;
        }
    }
    
}
