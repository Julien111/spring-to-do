/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tutotest.demotodo.service;

import com.tutotest.demotodo.entity.ToDoList;
import com.tutotest.demotodo.repository.ToDoListRepository;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Julien
 */
@Service
public class ToDoListService {
    
    @Autowired
    private ToDoListRepository toDoListRepository;   
    
    public List<ToDoList> getToDoLists(){
        List<ToDoList> toDo = StreamSupport
        .stream(toDoListRepository.findAll().spliterator(), false) //
        .collect(Collectors.toList());        
        return toDo;
    }
        
    public ToDoList getToDoById(long id) {
        ToDoList result = toDoListRepository.findById(id);
        if(result == null){
            return null;
        }
        return result;
    }
    
    public boolean existsById(long id){
        return toDoListRepository.existsById(id);
    }
    
    public void createTask(ToDoList task){
        this.toDoListRepository.save(task);
    }
    
    public void updateTask(ToDoList task){
        this.toDoListRepository.save(task);
    }
    
    public void deleteTask(ToDoList task){
        this.toDoListRepository.delete(task);
    }
}
