/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.tutotest.demotodo.service;

import com.tutotest.demotodo.entity.User;
import com.tutotest.demotodo.exception.UserAlreadyExistException;
import com.tutotest.demotodo.web.dto.UserRegistrationDto;

/**
 *
 * @author Julien
 */
public interface UserService {
    User save(UserRegistrationDto registrationDto) throws UserAlreadyExistException;

    boolean checkIfUserExist(String email);
}
